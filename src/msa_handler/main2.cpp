#include "MSA.h"
#include "MSA_cumulative.h"
#include <fstream>
#include <iostream>
#include <string>

#define THRESHOLD 70
#define PLOIDITY 2


using namespace std;

int main(int argc, char** argv){
  if (argc == 1) {
    cerr << "Usage :\n MTool input.fasta [output_name [threshold]]" << endl;
    exit(-1);
  }
  if (argc > 1) {
    MSA_cumulative msa_fasta = MSA_cumulative();
    msa_fasta.parser_fasta(argv[1]);
    ofstream output;
    string filename;
    if (argc > 2) {
      filename = argv[2];
    }
    else
      filename = "output.txt";
    output.open (filename, ios::out | ios::app | ios::ate);
    string cons_name = argv[1]; 
    int index = cons_name.find_last_of('/') +1;
    int threshold;
    if (argc > 3)
      threshold = stoi(argv[3]);
    else
      threshold = THRESHOLD;
    int ploidity;
    if (argc > 4)
      ploidity = stoi(argv[4]);
    else
      ploidity = PLOIDITY;
    cons_name = ">lcl|consensus_"+cons_name.substr(index) + " threshold = " + to_string(threshold);
    output << cons_name << endl << msa_fasta.consensus(threshold, ploidity) << endl;
    output.close();
  }
}
