#ifndef MSA_CUMUL_H
#define MSA_CUMUL_H

#include "MSA.h"

class MSA_cumulative : public MSA
{
 public:
  string consensus(int threshold, int ploidity);
};

#endif
