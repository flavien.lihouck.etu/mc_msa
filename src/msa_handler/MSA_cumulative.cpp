#include "MSA_cumulative.h"
#include <algorithm>


string MSA_cumulative::consensus(int threshold, int ploidity) {
  string consensus_seq("");
  int size_alpha = int(alphabet.size());
  int current_weight(1);
  for (int i(0); i < length; ++i) {


    int size_bl = int(text.size());
    int scores[size_alpha] = {0};
    int depth_index = 0;
    int alpha_index = 0;
    int highest_score =0;
    while (depth_index < size_bl && threshold*current_weight > (highest_score*100)/size_bl) {
      alpha_index = alphabet.find(text[depth_index][i]);
      if (alpha_index != -1) {
	scores[alpha_index] += current_weight;
      }
      else {
	vector<char> multiple_basis = IUPAC_to_basis(text[depth_index][i]);
	int size = multiple_basis.size();
	if (current_weight % size != 0) {
	  current_weight *= size;
	  for( int k(0); k <(int)alphabet.size();++k) {
	    scores[k] *= size;
	  }
	}
	for (auto it = begin(multiple_basis); it != end(multiple_basis); ++it) {
	  scores[alphabet.find(*it)] += (current_weight / size);
	}
      }
      highest_score = max(highest_score, scores[alpha_index]);
      
      //cerr << "index " <<alpha_index << " score " << highest_score << " percentage : " << (highest_score*100)/size_bl << endl;
      depth_index++;
    }
    if (depth_index < size_bl){
      //cerr << "char at pos " << i << " is " << alphabet[alpha_index] << " end pos is " << length << endl;
      if (alpha_index != 0 && alpha_index != 16)
	consensus_seq += alphabet[alpha_index];
    }
    else {
      vector<char> conserved_nuc;
      int total_score(0);
      while (conserved_nuc.size() < 6 && threshold*current_weight > (total_score*100)/size_bl) {
	    alpha_index = 0;
	    while (alpha_index < size_alpha && scores[alpha_index] < highest_score) {
	        alpha_index++;
	    }
	    total_score += scores[alpha_index];
	    conserved_nuc.insert(conserved_nuc.begin(), alphabet[alpha_index]);

	/*for (int k(0); k < size_alpha; k++)
	  scores[k] += highest_score;*/
	    scores[alpha_index] = 0;
	    auto it = max_element(scores, scores+size_alpha);
        highest_score = *it;
      }

      char nuc = basis_to_IUPAC(conserved_nuc);
      if (nuc != '-' && nuc != '.')
        consensus_seq += nuc;
    }
  }
  return consensus_seq;
}
