#ifndef MSA_H
#define MSA_H

#include <string>
#include <vector>
#include <iostream>
#include <fstream>

using namespace std;

class MSA
{
 public:
  vector<string> text;
  vector<string> name;
  string alphabet=".ACGTN-acgtn";
  // The IUPAC alphabet has been organised so that the element matches the binary code of - : (2^4) , T : (2^3), G : (2^2), C : (2^1), A : (2^0). This way, a mask is easy to create.
  string alphabet_IUPAC=".ACMGRSVTWYHKDBN-acmgrsvtwyhkdbn";
  int current_weight;
  int length;
  int lines;

  //Takes a vector of basis and returns the matching (extended ?) IUPAC character.
  char basis_to_IUPAC(vector<char> basis);
  vector<char> IUPAC_to_basis(char code);
  void parser_fasta(string file);
  string consensus(int threshold, int ploidity);
  void add_sequence(const string& str);


  MSA() {
    length = 0;
    lines = 0;
    current_weight = 1;
  };

};
#endif
