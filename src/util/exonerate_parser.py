#!/usr/bin/env python3

# # This script aims to extract the mismatches found by exonerate from the matching reads region, creating a file of
# all the problems with their "origin" region #

import sys

DEFAULT_DISTANCE = 10
LINE_HEADER_SIZE = 9


def get_region_file(regions, folder, value, format_func="{0}/read_r{1}.fasta".format):
    """
    Find the read file
    :param regions: (list/string) the list of every regions format to look for in
    :param folder: the read regions' folder
    :param value: the position to extract from target file
    :param format_func: the read regions' file naming format
    :return:
    """
    target_region = ""
    if not isinstance(regions, list):
        regions = regions.split(",")
    if isinstance(regions, list) and len(regions) == 1:
        region_list = regions[0].replace("[", "").replace("]", "").split(" ")
    else:
        region_list = regions
    for elem in region_list:
        val = elem.split("_")
        if int(val[0]) <= value < int(val[1]):
            target_region = elem

    return format_func(folder, target_region) if target_region != "" else ""


def extract_read_pile(file, position, distance, output, padding):
    """
    Extracts every reads matching the region from file and writes them into the output file
    :param file: the reads file
    :param position: the mismatch position
    :param distance: the size of elements to pick before and after the position
    :param output: the output file to write in
    :param padding: the padding length to add at the beginning of each line
    :return: the amount of reads written
    """
    line_cnt = 0
    with open(file, "r") as input:
        for line in input:
            output.write(" "*padding)
            if line_cnt % 2 == 0:
                output.write(line)
            else:
                start = position - distance if position - distance > 0 else 0
                end = position + distance if position + distance < len(line) else len(line)
                output.write(line[start:end].strip("\n")+"\n")
            line_cnt += 1
    output.write("\n\n")
    return line_cnt


def main():
    if len(sys.argv) < 7:
        print("Usage: ./exonerate_parser.py INPUT OUTPUT OUTPUT_LENGTH READ_REGION_LENGTH READ_REGION_FOLDER READ_REGION_LIST\n")
        exit("Missing parameters")
    if len(sys.argv) > 3:
        distance = int(sys.argv[3])
    else:
        distance = DEFAULT_DISTANCE

    size = int(sys.argv[4])
    folder = sys.argv[5]
    regions = sys.argv[6].strip("[").strip("]").split(",")
    with open(sys.argv[1], 'r') as f:
        lines = [line.replace("\n","") for line in f][12:-2]
        line_cnt = 1
        problems = []
        print(lines[0].find(":"))
        header_size = lines[0].find(": ")+2
        while line_cnt < len(lines):
            char_cnt = header_size
            while char_cnt < len(lines[line_cnt])-header_size:
                if lines[line_cnt][char_cnt] != "|":
                    problems.append((line_cnt, char_cnt-header_size+1))
                    char_cnt += distance
                else:
                    char_cnt += 1
            line_cnt += 4
        with open(sys.argv[2], 'w') as output:
            pb_cnt = 1
            for pb in problems:
                x, y = pb
                target = lines[x-1].split(":")
                query = lines[x+1].split(":")
                target[1] = target[1].strip(" ")
                query[1] = query[1].strip(" ")
                value1 = int(target[0])
                value2 = int(query[0])
                if y - distance < 0:
                    start = 0
                    prefix_start = y - distance
                else:
                    start = y - distance
                    prefix_start = None
                if prefix_start and x > 1:
                    prefix_x = x-4
                    prefix_1 = lines[prefix_x-1].split(":")[1][prefix_start:-1]+"/"
                    prefix_2 = lines[prefix_x][prefix_start:-1]+"/"
                    prefix_3 = lines[prefix_x+1].split(":")[1][prefix_start:-1]+"/"
                else:
                    prefix_x = None
                    prefix_1, prefix_2, prefix_3 = ("", "", "")
                if y + distance > len(target[1]):
                    end = len(target[1])-1
                    suffix_end = y+distance - len(target[1])
                else:
                    end = y + distance
                    suffix_end = None
                if suffix_end and x < len(lines) - 2:
                    suffix_x = x+4
                    suffix_1 = "\\"+lines[suffix_x-1].split(":")[1][1:suffix_end+1]
                    suffix_2 = "\\"+lines[suffix_x][header_size:suffix_end+header_size]
                    suffix_3 = "\\"+lines[suffix_x+1].split(":")[1][1:suffix_end+1]
                else:
                    suffix_x = None
                    suffix_1, suffix_2, suffix_3 = ("","", "")
                output.write("Error {0} : Target position = {1} Query position = {2}\n{3}{4}{5}\n".format(str(pb_cnt), str(value1+start), str(value2 + start), prefix_1, target[1][start:end], suffix_1))
                output.write("{0}{1}{2}\n".format(prefix_2, lines[x][start+header_size:end+header_size], suffix_2))
                output.write( "{0}{1}{2}\n\n".format(prefix_3, query[1][start:end], suffix_3))
                pb_cnt += 1
                file = get_region_file(regions, folder, y+value2)
                output.write(file+ "\n\n")
                extract_read_pile(file, value2 % size, distance+2, output, 0)

if __name__ == "__main__":
    main()