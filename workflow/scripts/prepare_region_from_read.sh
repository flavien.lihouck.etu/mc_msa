#!/bin/bash

# This script takes a read pile in the sam format, and creates regions where the depth is at least equal to a minimum
# input: $1 A read pile in sam format
#        $2 The path for the final output
#        $3 The minimum depth
# This script requires samtools to function

INPUT=$1
OUTPUT=$2
MIN=$3
samtools sort -O sam $INPUT -o tmp.sam
samtools depth tmp.sam > depth.txt
cut -f2- depth.txt > depths_clean.txt 
python3 workflow/scripts/delimit_regions.py depths_clean.txt $OUTPUT $MIN
rm -f tmp.sam depth.txt depths_clean.txt


