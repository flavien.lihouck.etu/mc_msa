#!/bin/bash
input=$1/$2

file_cnt=0
create=0
while IFS= read -r line
do
    if [ $create == 0 ]
    then
	echo "$line" > "${1}/${file_cnt}_${2}"
	((create=1))
    else
	((create=0))
	echo "$line" >> "${1}/${file_cnt}_${2}"
	((file_cnt=file_cnt+1))
    fi

done < "$input"
	       
