#!/usr/bin/env python3

import sys



def find_nth(string, substring, n):
    if (n == 1):
        return string.find(substring)
    else:
        return string.find(substring, find_nth(string, substring, n - 1) + 1)


def main():
    args = sys.argv
    with open(args[1], "w") as output:
        with open(args[2], 'r') as file:
            lines = file.readlines()
            output.write(">"+lines[0][:find_nth(lines[0], "_", 3)]+"\n"+ lines[1][:-1])
        file_cnt = 3
        while file_cnt < len(args):
            with open(args[file_cnt], "r") as file:
                lines = file.readlines()
                output.write(lines[1][1:-1])
            file_cnt += 1

        output.write('\n')


if __name__ == "__main__" :
    main()
