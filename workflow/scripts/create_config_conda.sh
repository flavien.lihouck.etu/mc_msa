#!/bin/bash
LINE_NUMBER=`cat ~/.bashrc|grep -n "conda initialize"|cut -d":" -f1`

if [[ "$LINE_NUMBER" == "" ]]
then
  echo "Can't find the configuration to use conda in your ~/.bashrc."
  echo "Conda (>4.10) must be installed (see https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html)."
else
  START=`echo $LINE_NUMBER |cut -d" " -f1`
  END=`echo $LINE_NUMBER |cut -d" " -f2`
  head -$END ~/.bashrc|tail -$(($END-$START+1)) >workflow/scripts/config_conda.sh
fi
