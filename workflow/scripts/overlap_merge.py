#!/usr/bin/env python3

import sys


def main():
    args = sys.argv
    overlap = int(args[2])
    with open(args[1], "w") as output:
        with open(args[3], 'r') as file:
            lines = file.readlines()
            output.write(">meta_consensus\n"+ lines[1][:-1])
        file_cnt = 4
        while file_cnt < len(args):
            with open(args[file_cnt], "r") as file:
                lines = file.readlines()
                output.write(lines[1][1:-1])
            file_cnt += 1

        output.write('\n')


if __name__ == "__main__" :
    main()
