#!/usr/bin/env python3

import subprocess
import os
import sys, argparse

directory = "../data/msa"
region ="100"
dimension ="10"

def main() :
    pattern = ""
    parser=argparse.ArgumentParser()
    parser.add_argument("-directory", help="target directory", required=True)
    parser.add_argument("-output", help="output name")
    parser.add_argument("-region", help="MSA region")
    parser.add_argument("-depth", help="MSA depth")
    parser.add_argument("-threshold", help="consensus threshold")
    parser.add_argument("-ploidity", help="expected ploidity")
    args = parser.parse_args()
    if args.output:
        output = args.output
    else:
        output = "default.txt"
    if args.region:
        pattern += "r"+args.region+"_"
    if args.depth:
        pattern += "d"+args.depth+"."
    for filename in os.listdir(args.directory) :
        command = ["./msa_handler/MTool", args.directory+"/"+filename, output]
        if args.threshold:
            command.append(args.threshold)
        if pattern in filename and "fasta" in filename and not "_poa_" in filename:
            subprocess.run(command, capture_output=True).stdout


if __name__ == "__main__":
    main()
