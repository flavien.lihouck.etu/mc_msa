#!/usr/bin/env python3

import subprocess
import os
import sys , argparse

config_file = "../config.yaml"

def main() :
    pattern = ""
    parser = argparse.ArgumentParser(description="Creates the config file for Meta-consensus pipeline")
    parser.add_argument("-input", help="Reads file", required=True)
    parser.add_argument("-output", help="Target directory for the pipeline results", required=True)
    parser.add_argument("-reference", help="Reference for alignment and statistics")
    parser.add_argument("-size", help="The size for cutting region (default: maximum size)", default='0')
    parser.add_argument("-list", help="A list of regions to work on (format: [r1, r2, ...] or [rStart_End]) (default: no region)")
    parser.add_argument("-tools", help="The list of tools to use in the meta-consensus (default: ['abpoa', 'spoa', 'kalign2', 'kalign3', 'mafft', 'muscle'])", default="['abpoa', 'spoa', 'kalign2', 'kalign3', 'mafft', 'muscle']")
    parser.add_argument("-consensus_threshold", help="Threshold(s) used for the MSA consensus step (default:  [70])", default="[70]")
    parser.add_argument("-metaconsensus_threshold", help="Threshold(s) used for the Meta-consensus result (default: [60])", default="[60]")
    parser.add_argument("-depth", help="The depth used in the process (default: [80])", default="[80]")
    parser.add_argument("-plot", help="Analyse the meta-consensus and MSA consensus quality (requires reference)")
    parser.add_argument("-region_overlap", help="The size of the overlap between regions", default="50")
    args = parser.parse_args()

    with open(config_file, 'w') as c_file:
        c_file.write('read_file: ' + args.input + "\n")
        c_file.write("output_folder: "+args.output + "\n")
        if args.reference:
            c_file.write("ref_file: "+args.reference+"\n")
        region_param = False
        if args.list:
            r = args.list
            r = r.replace("[", "")
            r = r.replace("]","")
            regions = r.split(",")
            actual_regions = "["
            last_elem = None
            for elem in regions:
                if '_' in elem:
                   actual_regions+= elem +","
                elif args.size != 0:
                    actual_regions+= elem+"_"+str(int(elem)+int(args.size))+","
                elif last_elem:
                    actual_regions+= last_elem+'_'+elem+","
                    last_elem = None
                else:
                    last_elem = elem
            print(actual_regions)
            c_file.write("region: "+actual_regions[:-1]+"]\n")
        else:
            c_file.write("size: " + args.size+"\n")
            c_file.write("region_overlap: "+ args.region_overlap+"\n")
        c_file.write("tool: "+ args.tools+ "\n")
        c_file.write("threshold_1: "+ args.consensus_threshold +"\n")
        c_file.write("threshold_2: "+ args.metaconsensus_threshold +"\n")
        c_file.write("depth: "+args.depth +" \n")


if __name__ == "__main__":
    main()

