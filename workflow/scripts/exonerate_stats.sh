#!/bin/bash

printf "" > $1
for FILE in $@ ;
do
  if [ $FILE != $1 ] && [ $FILE != $2 ] ;
  then
  printf "%q," $FILE  >> $1
    exonerate --bestn 1 -Q dna -E -m a:g --showalignment false --showsugar false --showvulgar false --showcigar false  --ryo "%pi, %ps, %et, %em, %tl, %tal\n"  --verbose 0 -t $2 -q $FILE >> $1 2>/dev/null ;
  fi;
done

