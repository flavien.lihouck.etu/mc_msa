#!/bin/bash

# This script splits a region from a reference (or anchor read)
# input $1 (int) the start of the region
#       $2 (str) the path to the reference (fasta) to cut
#       $3 (str) the path for the output file (fasta)
#       $4 (int) the size of the region

if [ "$#" -lt 3 ]
then
    echo 'USAGE: ./seq_region.sh {input_start} {input_ref} {output} {region_size}'
else
    INPUT_START=$1
    INPUT_REF=$2
    OUTPUT=$3
    REGION_SIZE=$4
    let "START_REGION= $INPUT_START"
    let "END_REGION = $REGION_SIZE"
    cat $INPUT_REF |grep ">" >$OUTPUT
    cat $INPUT_REF |grep -v ">"|tr -d "\n"|cut -c $START_REGION-$END_REGION >>$OUTPUT
fi
