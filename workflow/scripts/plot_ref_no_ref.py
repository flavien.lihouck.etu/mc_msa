#!/usr/bin/env python3


import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import re
import sys
import os

def parse_file(filename, x_scale):
    identity, similarity, equivalence, mismatches, length, aln_length = [ [], [] , [], [], [],[]]
    value = []

    with open(filename, 'r') as f:
        cnt= 0
        for line in f:
            try:
                i, s, e, m, tl, al = map(float, line.split(',')[1:])
            except:
                i,s,e,m, tl, al = [0, 0, 0, 0, 0, 0]
                for x in range(1, len(line.split(','))-1):
                    print("Missing values replaced by 0, 0, 0, 0, 0, 0")

            identity.append(i)
            similarity.append(s)
            equivalence.append(e)
            mismatches.append(m)
            length.append(tl)
            aln_length.append(al)
            cnt+=1
    return identity, similarity, equivalence, mismatches, length, aln_length



def plot_from_folder(args, target, labels, regions, folder):
    ls, li, le, lm, ltl, lal = [[0 for r in regions] for k in range(6)]
    nls, nli, nle, nlm, nltl, nlal = [[0 for r in regions] for k in range(6)]
    for tool_file in args:
        if folder != "":
            cond = ".txt" in tool_file.name
            filename = folder+tool_file.name
        else:
            cond = ".txt" in tool_file
            filename= tool_file
        if cond :
            i, s, e, m, tl, al = parse_file(filename, regions)
            region_index = 0
            for r in regions:
                if r in filename:
                    if 'no_ref' in filename:
                        nli[region_index] = i.pop()
                        nls[region_index] = s.pop()
                        nle[region_index] = e.pop()
                        nlm[region_index] = m.pop()
                        nltl[region_index] = tl.pop()
                        nlal[region_index] = al.pop()
                    else:
                        li[region_index] = i.pop()
                        ls[region_index] = s.pop()
                        le[region_index] = e.pop()
                        lm[region_index] = m.pop()
                        ltl[region_index] = tl.pop()
                        lal[region_index] = al.pop()
                region_index += 1

    ll = [[ls, nls],  [li, nli], [le, nle], [lm, nlm], [ltl, nltl], [lal, nlal]]
    graph_cnt = 0
    target_att = ["identity", "similarity", "equivalence", "mismatches", "sequence_length", "aligned_length"]
    arr = np.arange(len(regions))
    for graph in ll:
        fig = plt.figure(figsize=(50,25))
        tab_cnt=0

        N = np.arange(len(regions))
        ax = fig.add_axes([0.1,0.1,0.8, 0.8])
        x_label = ["ref","no_ref"]
        ax.set_ylabel(target_att[graph_cnt])
        ax.set_xticks
        region_cnt = 0
        offset_cnt = 0.00
        x_label_cnt = 0
        for tab in graph:
            ax.bar(arr + offset_cnt, tab, label= x_label[x_label_cnt], width=0.25)
            offset_cnt += 0.25
            if x_label_cnt > 0:
                region_cnt+= 1
            x_label_cnt += 1
                #    plt.plot(th,tab, label=labels[tab_cnt])
         #   else:
          #      plt.plot(th,[tab for x in range(len(th))], label=labels[tab_cnt])
            tab_cnt+=1
        plt.xticks(arr,)
        legend = plt.legend(shadow=True, fontsize='x-large')
        plt.savefig(target+"_" + target_att[graph_cnt] + ".png", format="png")
        plt.cla()
        plt.clf()
        plt.close()
        graph_cnt+=1

if __name__ == "__main__":
    tmp = sys.argv[3].replace('"', "")
    tmp = tmp.replace('[', "")
    tmp = tmp.replace(']', "")

    region = list(tmp.split(","))[:-1]
    print(region)

    if (len(sys.argv) < 4):
        exit(-1)
    if (len(sys.argv) < 4):
        print(sys.argv)

        folder_it = os.scandir(sys.argv[3])
        name= [ x.name for x in folder_it]
        #folder_it= os.scandir(sys.argv[2])
        plot_from_folder(folder_it, sys.argv[1], name, folder=sys.argv[3])
        exit(0)
    lb= []
    #for elem in th:
     #   lb.append("th "+str(elem))
    plot_from_folder(sys.argv[4:] ,sys.argv[1],sys.argv[4:], region, folder="")
