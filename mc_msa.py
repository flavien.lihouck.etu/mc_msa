#!/usr/bin/env python3

import subprocess
import os
import sys
import argparse



config_file = "workflow/config.yaml"

def create_config() :
    pattern = ""
    parser = argparse.ArgumentParser(usage="mc_msa [--help] -i INPUT -o OUTPUT [-r REFERENCE -t TOOLS ...]\n", description="Creates the config file then runs the Meta-consensus pipeline", add_help=False)
    required = parser.add_argument_group("Required arguments")
    basic = parser.add_argument_group("Standard arguments")
    advanced = parser.add_argument_group("Advanced arguments")
    basic.add_argument("-h", "-help", action="help", help="show this help message and exit")
    required.add_argument("-input", "-i", help="Reads file", required=True)
    required.add_argument("-output", "-o", help="Target directory for the pipeline results", required=True)
    basic.add_argument("-reference", "-r", help="Reference for alignment and statistics")
    #advanced.add_argument("-list", help="A list of regions to work on (format: [r1, r2, ...] or [rStart_End, ...]) (default: no region)")
    advanced.add_argument("-size", "-s", help="The desired region size (default: maximum)", default='0')
    basic.add_argument("-tools", "-t", help="The list of tools to use in the meta-consensus (default: ['abpoa', 'spoa', 'kalign2', 'kalign3', 'mafft', 'muscle'])", default="['abpoa', 'spoa', 'kalign2', 'kalign3', 'mafft', 'muscle']")
    advanced.add_argument("-consensus_threshold", "-ct", help="Threshold(s) used for the MSA consensus step (default:  [50])", default="[50]")
    advanced.add_argument("-metaconsensus_threshold", "-mt",  help="Threshold(s) used for the Meta-consensus result (default: [50])", default="[50]")
    advanced.add_argument("-depth", "-d", help="The depth used in the process (default: max)", default="0")
    #parser.add_argument("-plot", help="Analyse the meta-consensus and MSA consensus quality (requires reference)", action='store_true')
    #advanced.add_argument("-region_overlap", help="The size of the overlap between regions", default="0")
    basic.add_argument("-cores", "-c", help="The amount of cores to use in the pipeline run (default 1)", default=1)
    args = parser.parse_args()

    with open(config_file, 'w') as c_file:
        c_file.write('read_file: ' + args.input + "\n")
        if args.output[-1] == "/":
            output = args.output[:-1]
        else:
            output = args.output
        c_file.write("output_folder: "+ output + "\n")
        if args.reference:
            c_file.write("ref_file: "+args.reference+"\n")
        region_param = False
 #       if args.list:
 #           r = args.list
 #           r = r.replace("[", "")
 #           r = r.replace("]","")
 #           regions = r.split(",")
 #           actual_regions = "["
 #           last_elem = None
 #           for elem in regions:
 #               qt = "" if "'" in elem else "'"
 #               if '_' in elem:
 #                   actual_regions+= qt+ elem.replace('"', "'").replace(" ", "") +qt +","
 #               elif args.size != 0:
 #                   actual_regions+= qt+ elem.replace('"', "'").replace(" ", "")+"_"+str(int(elem)+int(args.size))+ qt+ ","
 #               elif last_elem:
 #                   actual_regions+= qt+last_elem+'_'+elem.replace('"', "'").replace(" ", "")+qt+","
 #                   last_elem = elem
 #               else:
  #                  last_elem = elem
   #         actual_regions = actual_regions[:-1]
    #        c_file.write("region: "+actual_regions+"]\n")
     #   else:
        c_file.write("region_size: " + args.size+"\n")
    #    c_file.write("region_overlap: "+ args.region_overlap+"\n")
        c_file.write("tool: "+ args.tools+ "\n")
        c_file.write("threshold_1: "+ args.consensus_threshold +"\n")
        c_file.write("threshold_2: "+ args.metaconsensus_threshold +"\n")
        c_file.write("depth: "+args.depth +" \n")

        plot = ''
        if args.reference:
            plot = 'plot'
        return {"core": str(args.cores), "plot" : plot}


def config_conda():
    try:
        open("workflow/scripts/config_conda.sh")
    except:
        result = subprocess.run("./workflow/scripts/create_config_conda.sh")
        try:
            open("workflow/scripts/config_conda.sh")
        except:
            print("Unable to launch the pipeline.")
            sys.exit()


def main():
    config_conda()
    data = create_config()
    result = subprocess.run(["./workflow/scripts/snakemake_launcher.sh",os.path.join("workflow","config.yaml"), data["core"] , data['plot']])

if __name__ == "__main__":
    main()